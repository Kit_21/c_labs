#include <stdio.h>
#include <math.h>

int main()
{
    double x;
    printf("Enter x { 0.25 <= x <= 0.523 } -> ");
    scanf("%lf", &x);
    if (0.25 <= x && x <= 0.523)
    {
        double y = acos(2 * sin(x));
        double z = sqrt(cos(pow(y,2)));
        printf("y(x) = %lf\nz(y) = %lf\n", y, z);
    }
    else
        printf("x value is incorrect!\n");
    return 0;
}
