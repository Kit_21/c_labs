
#include <stdio.h>
#include <math.h>

int main()
{
    double h, y, x;
    int n=1;
    printf("Enter h -> ");
    scanf("%lf", &h);
    
    int i = floor(0.5/h) + 1;
    printf("%d\n", i);
    printf("x\t\tf(x)\n");
    
    
    do
    {
        if(n<=i/2)
        {
            y = exp(sin(x));
        }
        
        else
        {
            y = exp(x) - pow(x, -0.5);
        }
    
        printf("%lf\t%lf\n", x, y);
        x+=h;
        n+=1;
    } while (n<=i);

    return 0;
}
