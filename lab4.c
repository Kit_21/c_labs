#include <stdio.h>
#include <math.h>

int main()
{
    double d, e, h, y, x, a = 0, b = 0.5, f, I, first = 0;
    int i, k, n;

    printf("Enter E -> ");
    scanf("%lf", &e);

    for(n = 1; ; n *= 2)
    {
        h = (b - a) / n;
        k = floor(0.5 / h) + 1;
        
        for (i = 0; i < k; i++)
        {
            x = a + i * h + h*0.5;
            if (x<=0.25)
                y = exp(sin(x));
            else
                y = exp(x) - pow(x, -0.5);
            f += y;
    
        }

        I = h * f;
        f = 0;
        
        if(first)
        {
            d = fabs(I - first) / 3;
            if(d < e)
                break;
        }
        first = I;
    }
    printf("I = %lf\n", I);
    return 0;
}