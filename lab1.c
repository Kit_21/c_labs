#include <stdio.h>
#include <math.h>

int main()
{
    float x, y, z, maxi, s;
    printf("Enter x -> ");
    scanf("%f", &x);
    printf("Enter y -> ");
    scanf("%f", &y);
    printf("Enter z -> ");
    scanf("%f", &z);

    if (!x && !y && !z)
        return 0;
    
    if (x<y && z<y)
        maxi = y;
    else if (x<z)
        maxi = z;
    else maxi = x;

    s = x + y + z;

    if (maxi > s - maxi)
        printf("Maximum: %f\n", maxi);
    else printf("Difference between sum and maximum: %f\n", (s - maxi) - maxi);

    return 0;
} 
